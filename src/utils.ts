interface rect {
    width: number
    height: number
}
const getRect = (image: HTMLImageElement): Promise<rect> => {
    return new Promise(resolve => {
        if (image.width) {
            return resolve({
                width: image.width,
                height: image.height
            })
        }
        setTimeout(() => {
            getRect(image)
        }, 300);
    })
}

export class CompressImage {
    size: number = 300000
    constructor(size?: number) { }
    async compress(file: string): Promise<string> {
        const img: HTMLImageElement = new Image(), can: HTMLCanvasElement = document.createElement('canvas'), ctx: CanvasRenderingContext2D = can.getContext('2d')!
        img.src = file
        const { width, height } = await getRect(img)
        can.width = width
        can.height = height
        ctx.fillRect(0, 0, width, height)
        ctx.drawImage(img, 0, 0, width, height)
        return Promise.resolve(can.toDataURL('image/jpeg', 0.5))
    }
}