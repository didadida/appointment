import Vue, { VNode } from 'vue';

declare global {
  interface chatItem {
    id?: number;
    date: number;
    userId?: number;
    avatar?: string;
    gender?: number;
    text: string,
    from: number,
    dateString?: string,
    loading?: boolean,
    fail?: boolean,
    giftName?: string,
    giftType?: number,
  }

  interface giftItem {
    id: number,
    pic: string,
    gifPic: string,
    name: string,
    price: string | number
  }

  interface uploadImage {
    content: string
    file: File
  }

  namespace JSX {
    // tslint:disable no-empty-interface
    interface Element extends VNode { }
    // tslint:disable no-empty-interface
    interface ElementClass extends Vue { }
    interface IntrinsicElements {
      [elem: string]: any;
    }
  }
}
