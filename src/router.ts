import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/home.vue';
import store from './store';

Vue.use(Router);
const loadComponent = (view: string): Function => () =>
  import(`./views/${view}`);

const router: Router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: '遇TA',
      component: Home,
      meta: { footerBarHide: true },
    },
    {
      path: '/guide',
      name: '遇TA',
      component: loadComponent('guide'),
    },
    {
      path: '/register',
      name: '注册',
      component: loadComponent('register'),
    },
    {
      path: '/profile',
      name: '个人资料',
      component: loadComponent('profile'),
    },
    {
      path: '/table',
      name: 'table',
      component: loadComponent('table'),
    },
    {
      path: '/greet',
      name: '招呼',
      component: loadComponent('greet'),
    },
    {
      path: '/chat',
      component: loadComponent('chat'),
      meta: { footerBarHide: true },
    },

    {
      path: '/message',
      name: '消息',
      component: loadComponent('message'),
    },

    {
      path: '/gifts',
      name: '我的礼物',
      component: loadComponent('gifts'),
    },

    {
      path: '/withdrawal',
      name: '提现',
      component: loadComponent('withdrawal'),
    },

    {
      path: '/mine',
      name: '个人中心',
      component: loadComponent('mine'),
    },
    {
      path: '/feedback',
      name: '意见反馈',
      component: loadComponent('feedback'),
    },
    {
      path: '/refund',
      name: '申请退款',
      component: loadComponent('refund'),
    },

    {
      path: '/order',
      name: '订单信息',
      component: loadComponent('order'),
    },
    {
      path: '*',
      name: '出错了',
      component: loadComponent('404'),
    },
  ],
});

router.afterEach(to => {
  if (to.name) {
    document.title = to.name;
  }
  console.error(to.meta);
  console.error(to.meta.footerBarHide);
  store.dispatch('toggleFooterBar', to.meta.footerBarHide);
});

export default router;
