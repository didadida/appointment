import Vue, { VNode } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import Vant from 'vant';
import './styles/app.less';
import './styles/app.styl';
import './styles/iconfont.css';

Vue.config.productionTip = false;
Vue.component('icon', {
    render(c) {
        return c('van-icon', {
            attrs: {
                'class-prefix': 'icon',
            },
            props: {
                size: this.size,
                name: this.name
            },
            on: {
                click: () => this.$emit('click')
            },
        })
    },
    props: ['size', 'name']
})
Vue.use(Vant);
new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');
