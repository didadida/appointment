const pic = require("@/assets/gift.png")
const gift = require("@/assets/gift.gif")
export const chatData: chatItem[] = [
    {
        id: 1,
        date: 1554594709,
        userId: 1,
        avatar: '',
        gender: 0,
        text: '你有几个小孩？',
        from: 0
    },

    {
        id: 2,
        date: 1554594722,
        userId: 1,
        avatar: '',
        gender: 1,
        text: '五个',
        from: 1
    },

    {
        id: 3,
        date: 1554594748,
        userId: 1,
        avatar: '',
        gender: 1,
        text: '他们叫甚么名字',
        from: 0
    },

    {
        id: 4,
        date: 1554605509,
        userId: 1,
        avatar: '',
        gender: 1,
        text: '晓明、晓明、晓明、晓明、晓明。',
        from: 1,
        giftName:'小心心',
        giftType:1
    },
    {
        id: 5,
        date: 1554605600,
        userId: 1,
        avatar: '',
        gender: 1,
        text: '都叫晓明，那你要叫他们吃饭时怎么办？',
        from: 0
    },
    {
        id: 6,
        date: 1554713405803,
        userId: 1,
        avatar: '',
        gender: 1,
        text: '要连续叫五声晓明吗?',
        from: 0
    },
    {
        id: 7,
        date: 1554713405803,
        userId: 1,
        avatar: '',
        gender: 1,
        text: '那很简单，我只要叫晓明，他们五个都会来。',
        from: 1
    },
    {
        id: 8,
        date: 1554713405803,
        userId: 1,
        avatar: '',
        gender: 1,
        text: '但是如果你只要叫某一特定小孩时，怎么办？',
        from: 0,
        giftName: '大心心',
        giftType:0
    },
    {
        id: 9,
        date: 1554713405803,
        userId: 1,
        avatar: '',
        gender: 1,
        text: '那更简单，只要加上他的姓就可以了。',
        from: 1,
        fail: true
    },
    {
        id: 10,
        date: 1554713405803,
        userId: 1,
        avatar: '',
        gender: 1,
        text: '我的网络不好，有点卡',
        from: 1,
        loading: true
    },
    {
        id: 11,
        date: 1554713405803,
        userId: 1,
        avatar: '',
        gender: 1,
        text: '我的网络不好，有点卡',
        from: 1,
        loading: true
    },
    {
        id: 12,
        date: 1554713405803,
        userId: 1,
        avatar: '',
        gender: 1,
        text: '我的网络不好，有点卡',
        from: 1,
        loading: true
    },
    {
        id: 13,
        date: 1554713405803,
        userId: 1,
        avatar: '',
        gender: 1,
        text: '我的网络不好，有点卡',
        from: 1,
        loading: true
    },
];

export const giftData: giftItem[] = [
    {
        id: 1,
        pic: pic,
        gifPic: gift,
        name: '小心心',
        price: 20
    },
    
    {
        id: 2,
        pic: pic,
        gifPic: gift,
        name: '大心心',
        price: 30
    },
    {
        id: 3,
        pic: pic,
        gifPic: gift,
        name: '520',
        price: 40
    },
    {
        id: 4,
        pic: pic,
        gifPic: gift,
        name: '撩一撩',
        price: 50
    },

]