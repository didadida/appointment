import Vue from 'vue';
import Vuex, { Store } from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    showfooterBar: false,
  },
  mutations: {},
  actions: {
    toggleFooterBar(store, value) {
      store.state.showfooterBar = value;
    },
  },
});
